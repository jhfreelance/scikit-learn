FROM ubuntu
COPY sources.list /etc/apt/
RUN \
    apt-get update \
    && apt-get install -y \
        build-essential \
        libatlas-dev \
        libatlas3-base \
        python-dev \
        python-setuptools \
        python \
        python-pip \
        python-numpy \
        python-scipy \
        python-pandas \
        python-matplotlib \
    && update-alternatives --set libblas.so.3      /usr/lib/atlas-base/atlas/libblas.so.3 \
    && update-alternatives --set liblapack.so.3    /usr/lib/atlas-base/atlas/liblapack.so.3 \
    && pip install -U scikit-learn
ENTRYPOINT [ "/usr/bin/python"]# See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
